class ArticleMailer < ApplicationMailer
  default from: "foo@bar.com"

  def new_article(sub_id, article_id)
    @article = Article.find(article_id)
    @subscription = Subscription.find(sub_id)
    mail({
         to: @subscription.email
    })
  end
end
