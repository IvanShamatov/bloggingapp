class ArticlesController < ApplicationController

  # GET /articles
  # GET /articles.json
  def index
    @articles = Article.order(updated_at: :desc)
  end

  # GET /articles/1
  # GET /articles/1.json
  def show
    @article = Article.friendly.find(params[:id])
  end

end
