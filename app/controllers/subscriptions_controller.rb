class SubscriptionsController < ApplicationController
  before_action :set_subscription, only: [:show, :destroy]

  # GET /subscriptions/1
  # GET /subscriptions/1.json
  def show
  end


  # POST /subscriptions
  # POST /subscriptions.json
  def create
    @subscription = Subscription.new(subscription_params)

    respond_to do |format|
      if @subscription.save
        format.html { redirect_to :back, notice: 'Subscription was successfully created.' }
        format.json { render :show, status: :created, location: @subscription }
      else
        format.html { render :back }
        format.json { render json: @subscription.errors, status: :unprocessable_entity }
      end
    end
  end


  # DELETE /subscriptions/1
  # DELETE /subscriptions/1.json
  def destroy
    @subscription.destroy
    respond_to do |format|
      format.html { redirect_to articles_url, notice: 'Subscription was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_subscription
      @subscription = Subscription.find_by(token: params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def subscription_params
      params[:subscription].permit(:email)
    end
end
