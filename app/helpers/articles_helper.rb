module ArticlesHelper
  def markdown(text)
    options = {:hard_wrap => true,
               :filter_html => true,
               :autolink => true, 
               :no_intra_emphasis => true, 
               :fenced_code => true, 
               :fenced_code_blocks => true,
               :gh_blockcode => true,
               :tables => true}
    engine = Redcarpet::Markdown.new(Redcarpet::Render::HTML, options)
    engine.render(text).html_safe
  end
end
