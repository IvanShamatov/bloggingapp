ActiveAdmin.register Article do
  
  permit_params :title, :content, :cover

  config.filters = false

  before_filter :only => [:show, :edit] do
    @article = Article.friendly.find(params[:id])
  end

  index do
    column :title do |article|
      link_to article.title, [:admin, article]
    end
    column :updated_at
    actions
  end

  form do |f|
    f.inputs do
      f.input :title
      f.input :cover
      f.input :content
    end
    f.actions
  end

  show do 
    attributes_table do
      row :title
      row :content do |article|
        markdown article.content
      end
      row :cover do |article|
        image_tag article.cover
      end
    end
  end

end
