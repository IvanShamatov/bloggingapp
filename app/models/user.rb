class User < ActiveRecord::Base

  devise :trackable, :omniauthable, :omniauth_providers => [:instagram]

  mount_uploader :avatar, ImageUploader

  has_many :comments


  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.name = auth.info.nickname   # assuming the user model has a name
      user.remote_avatar_url = auth.info.image # assuming the user model has an image
    end
  end
end
