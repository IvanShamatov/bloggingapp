class Article < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]

  after_create :send_notification

  mount_uploader :cover, ImageUploader

  has_many :comments

  scope :recent, -> {order(updated_at: :desc).limit(3)}

private
  def send_notification
    Subscription.delay.newsletter(self.id)
  end

end
