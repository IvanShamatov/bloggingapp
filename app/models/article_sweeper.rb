class ArticleSweeper < ActionController::Caching::Sweeper
  observe Article

  def after_save(article)
    expire_fragment "footer/recent_articles"
    expire_fragment "show/recent_articles"
    expire_cache article
  end

end