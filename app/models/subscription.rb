class Subscription < ActiveRecord::Base
  include Tokenable
  validates :email, uniqueness: true, email: true

  def self.newsletter(article_id)
    all.each do |sub|
      ArticleMailer.delay.new_article(email: sub.email, token: sub.token, article: article_id)
    end
  end

end
