== README



Features

* ~~Simple blog with articles.~~

* ~~The article body should be written and rendered with Markdown.~~

* ~~Articles should have comments.~~

* ~~Visitors should be able to subscribe to the site with their email.~~

* ~~Each time an article is posted, subscribers should be sent a notification.~~

* ~~Each email should contain a link to the article and another to unsubscribe.~~

* **Cover the code with test where you feel appropriate.**


Suggested spec extensions

* ~~SEO-friendly article URLs~~

* ~~An admin panel~~

* ~~Front-end styling and interaction~~

* ~~Asynchronous email delivery~~

* ** A development environment setup script**

* ~~HTTP / fragment caching~~


Constraints

* ~~Latest stable versions of Ruby and Rails~~

* ~~Erb rather than HAML~~

* ~~R-Spec rather than Test::Unit~~

* ~~Postgres rather than MySQL~~

* ~~Semantic, accessible HTML5~~

* ~~CoffeeScript rather than JavaScript~~

* ~~Sass rather than CSS~~


Для входа в админ-панель /admin/dashboard 

login: admin@example.com

pass: password


Через админку легко добавить пост, а в качестве cover можно использовать картинки из public. 


Успел не всё. Тесты откладывал до последнего, в итоге не хватило времени.

Из всего того, что нагенерил rspec я бы проверил:

* генерацию уникального токена для подписки (а точнее для отписки)

* наличие ссылок в письме с правильным урлом

* и отправку email'ов после создания Article


Я не проверил реально приходящие письма, а лишь увидел вызов мейлера в сайдкике. Соответственно, для запуска очереди нужно дополнительно запускать sidekiq.


Больше всего я потратил времени на красивый вид, и он мне хотя бы нравится.

Разобрался с кешированием, ну по крайней мере с fragment-caching. Посмотрел про HTTP-кеширование, но не было времени внедрить. 


Я так же поизучал тему с development environment setup script на примере Vagrant, но тоже руки не дошли.


Что еще хотелось бы сделать:

* Генерация мини-картинок для cover'a (просто добавить minimagick и дописать пару process'ов).

* Пагинацию (а лучше infinite scroll) как для статей, так и для комментариев

* Для комментариев добавить статус, а в админке возможность предмодерировать комментарии (spam/approved)