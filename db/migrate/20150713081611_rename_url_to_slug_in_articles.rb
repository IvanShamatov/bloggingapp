class RenameUrlToSlugInArticles < ActiveRecord::Migration
  def change
    rename_column :articles, :url, :slug
  end
end
