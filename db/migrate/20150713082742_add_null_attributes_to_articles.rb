class AddNullAttributesToArticles < ActiveRecord::Migration
  def change
    change_column_null :articles, :slug, false
    change_column_null :articles, :title, false
    add_index :articles, :slug, :unique => true
  end
end
