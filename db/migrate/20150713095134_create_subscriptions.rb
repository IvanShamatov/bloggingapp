class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.string :email
      t.string :token, unique: true

      t.timestamps null: false
    end
    add_index :subscriptions, :token, unique: true
  end
end
